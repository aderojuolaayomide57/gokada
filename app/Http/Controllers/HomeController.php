<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Validator;
use Illuminate\Http\Response;


use App\Models\PickUpRequest;


class HomeController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth')->except(['saveMapRequest', 'getSavedRequest']);
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Contracts\Support\Renderable
     */
    public function index()
    {
        return view('home');
    }


    public function saveMapRequest(Request $request)
    {
        $this->validate($request,[
        'result' => ['required', 'string','unique:pick_up_requests'],
        ]);
        $result = PickUpRequest::save_request($request->all());  
        return true;
    }

    public function getSavedRequest($search)
    {
      $result = PickUpRequest::getSavedRequest($search); 
      return response()->json(['location' => $result], 200);
    }
}
