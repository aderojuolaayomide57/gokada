<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class PickUpRequest extends Model
{
    use HasFactory;

    protected $fillable = ['address, longitude, latitude, results'];


    public static function save_request($request)
    {
        $pickuprequest = new pickuprequest();
        $pickuprequest->address = $request['address'];
        $pickuprequest->latitude = $request['latitude'];
        $pickuprequest->longitude = $request['longitude'];
        $pickuprequest->localGvt = $request['localGvt'];
        $pickuprequest->state = $request['state'];
        $pickuprequest->result = $request['result'];
        if($pickuprequest->save()){
            return true;
        } else{
            return false;
        }
    }


    public static function getSavedRequest($search)
    {
      //return PickUpRequest::orderBy('id', 'desc')
                //->where('address','LIKE','%'.$search.'%')
                //->get();

      return PickUpRequest::all();          
    }

}
