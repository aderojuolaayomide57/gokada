<?php

use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('welcome');
});

Auth::routes();

Route::get('/home', [App\Http\Controllers\HomeController::class, 'index'])->name('home');

Route::post('/saverequest', 
    [App\Http\Controllers\HomeController::class, 'saveMapRequest']
)->name('saverequest');

Route::get('/getrequest/{search}', 
    [App\Http\Controllers\HomeController::class, 'getSavedRequest']
)->name('getrequest');

