import React, { Component } from 'react';
import { Map, GoogleApiWrapper, InfoWindow, Marker } from 'google-maps-react';

const mapStyles = {
  width: '100%',
  height: '100%'
};

const GOOGLE_MAPS_APIKEY = 'AIzaSyCsDXd_Z09St8p1-px6gMDBRcB1dRbE1WY';



const processManualLocation = (props) => {
    if (props.userState !== "" && props.userCity !== "") {
        let city = props.userCity 
        let state = props.userPostalCode 
        let url = `https://maps.googleapis.com/maps/api/geocode/json?address=+${city},+${state}&key=${GOOGLE_MAPS_APIKEY}`
        fetch(url)
            .then(res => res.json())
            .then(res => {
                if (res.status === "OK") {
                    getUserCoords(res.results)
                } else if (res.status === "ZERO_RESULTS") {
                    alert('Unable to process this location. Please revise location fields and try submitting again.')
                }
            })
    } else {
        alert('Please ensure State and City are provided.')
    }
}

export class MapContainer extends Component {

    constructor(props){
        super(props);
        this.state = {
            showingInfoWindow: false,  // Hides or shows the InfoWindow
            activeMarker: {},          // Shows the active marker upon click
            selectedPlace: {}          // Shows the InfoWindow to the selected place upon a marker
        };

    }


  render() {
    return (
      <Map
        google={this.props.google}
        zoom={14}
        style={mapStyles}
        initialCenter={
          {
            lat: -1.2884,
            lng: 36.8233
          }
        }
      >

      </Map>
    );
  }
}

export default GoogleApiWrapper({
  apiKey: GOOGLE_MAPS_APIKEY
})(MapContainer);
 