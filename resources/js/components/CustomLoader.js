import React from 'react';


const CustomLoader = props => {

    return (
      <div className="customloader">
            <img 
                src={'images/loader.gif'}
            />
      </div>
    )
}



export default CustomLoader;