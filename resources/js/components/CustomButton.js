import React from 'react';
import { PropTypes } from 'prop-types';


const CustomButton = (props) => {
    return(
        <input 
            type='button'
            className='inputButton'
            value={props.value}
            onClick={props.onClick}
        />
    );
}

CustomButton.propTypes = {
    value: PropTypes.string,
    onClick: PropTypes.func,
}

export default CustomButton;