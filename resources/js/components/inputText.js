import React from 'react';
import { PropTypes } from 'prop-types';


const InputText = (props) => {
    return(
        <input 
            type='text'
            name={props.name}
            className='inputText'
            onChange={props.onChange}
            value={props.value}
            placeholder={props.placeholder}
            onClick={props.onClick}
        />
    );
}

InputText.propTypes = {
    name: PropTypes.string.isRequired,
    placeholder: PropTypes.string,
    onChange: PropTypes.func,
    value: PropTypes.string,
    onClick: PropTypes.func,
}

InputText.defaultProps = {
    placeholder: null,
}

export default InputText;