
import React from 'react';


const Modal = props => {
    return (
        <div className="overlay">
            <div className="overlay">
                <div className="overlay-content">
                    {props.children}
                </div>
            </div>
        </div>
        
    );
};


export default Modal;