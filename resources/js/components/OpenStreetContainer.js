import React, { Component } from 'react';
import { Map, TileLayer } from 'react-leaflet';
import { LatLngTuple } from 'leaflet';

const mapStyles = {
  width: '100%',
  height: '100%'
};

const defaultLatLng = [48.865572, 2.283523];
const zoom = 8;






 class OpenStreetContainer extends Component {

    constructor(props){
        super(props);
        

    }


  render() {
    return (
        <Map id="mapId"
            center={defaultLatLng}
            zoom={zoom}
        >

            <TileLayer
                url="https://{s}.tile.openstreetmap.org/{z}/{x}/{y}.png"
                attribution="&copy; <a href=&quot;http://osm.org/copyright&quot;>OpenStreetMap</a> contributors">
            </TileLayer>


        </Map>
    );
  }
}

export default OpenStreetContainer;
 