import React, { Component } from 'react';
import MapContainer from './MapContainer';
//import OpenStreetContainer from './OpenStreetContainer'
import Modal from './Modal';
import CustomLoader from './CustomLoader';
import CustomButton from './CustomButton'
import axios from 'axios';
import './mycss.css'

import InputText from './inputText';

class App extends Component {
    constructor(props){
        super(props);

        this.state = {
            pickup: 'Pickup address',
            dropoff: 'Dropoff address',
            active: '',
            modal: false,
            locations: [],
            loading: false,
            search: ''
        };
        this.handleChange = this.handleChange.bind(this);
        this.handleSubmit = this.handleSubmit.bind(this);
        this.fetchLiveAddress = this.fetchLiveAddress.bind(this);
        this.fetchLatandLng = this.fetchLatandLng.bind(this);
        this.handleOnClick = this.handleOnClick.bind(this);


    }

    handleChange(e) {
        this.setState({
            search: e.target.value,
            pickup: (this.state.active === 'pickup' && this.state.search !== '') ? e.target.value : this.state.pickup, 
            dropoff: (this.state.active === 'dropoff' && this.state.search !== '') ? e.target.value : this.state.dropoff 

        });
        this.fetchLiveAddress();
    }

    handleSubmit(e) {
        axios.post('/tasks', {
                dropoff: this.state.dropoff
            })
            .then(response => {
                console.log('from handle submit', response);
            });
    }

    handleOnClick(active){
        this.setState({
            modal: true, 
            active: active,
            search: '',
            locations: []
        })
    }

    handleOnSelect(active, data){
        const selected = `${data.address}, ${data.localGvt} ${data.state}`
        this.setState({
            search: selected, 
            dropoff: active === 'dropoff' ? selected : this.state.dropoff,
            pickup: active === 'pickup' ? selected : this.state.pickup

        })
    }

    fetchLiveAddress(e){
        this.setState({loader: true});
          axios.get(`/getrequest/search=${this.state.search}`)
          .then((response) => {
            this.setState({
              locations: [response.data.location],
              loader: false
            });
            //console.log(response.data.location);
        });
    }

    fetchLatandLng(e){
        this.setState({
            modal: !this.state.modal,
        });
        console.log(this.state.modal)
    }

    render(){
        return (
            <div className="container">
                <div className="row justify-content-center">
                    <div className="col-md-8">
                        <div className="headerDiv" >
                            <CustomButton 
                                value={this.state.pickup !== '' ? this.state.pickup : 'Pickup address'}
                                onClick={() => this.handleOnClick('pickup')}
                            />
                            <CustomButton 
                                value={this.state.dropoff !== '' ? this.state.dropoff: 'Dropoff address'}
                                onClick={() => this.handleOnClick('dropoff')}
                            />
                        </div>
                        <div className="mapDiv">
                            <MapContainer />
                            <div className="footerDiv" >
                                <div className="topDiv">
                                    <b>&#x20A6; 1500<span>,00</span></b>
                                    <b>3.3km | 24min</b>
                                </div>
                                <button>Enter Parcel Details</button>    
                            </div>
                        </div>
                    </div>
                </div>
                {this.state.modal &&   
                    <Modal>
                        <InputText 
                            name="search"
                            value={this.state.search}
                            placeholder={
                                this.state.active === 'dropoff' ? 'Dropoff address' : 'Pickup address'
                            }
                            onChange={this.handleChange}
                        />
                        <div className="innerModalContainer">
                            {this.state.active !== 'dropoff' &&
                                <div  className="addressWrapper">
                                    <i className='fa fa-map-marker'></i>
                                    <div>
                                        <b>Current Location</b>
                                    </div>
                                </div>}
                            {this.state.loader && <CustomLoader />}
                            {(!this.state.loader && this.state.locations !== []) &&  
                                <div>
                                    
                                {this.state.locations.map((data, index) => 
                                    <div key={index} 
                                        onClick={() => this.handleOnSelect(this.state.active, data[0])} 
                                        className="addressWrapper"
                                    >
                                        <i className='fa fa-map-marker'></i>
                                        <div>
                                            <b>{data[0].address}</b>
                                            <p>{data[0].localGvt}, {data[0].state}</p>
                                        </div>
                                    </div>)}
                                </div>
                            }
                        </div>
                        <button onClick={(e) => this.fetchLatandLng(e)}>Enter Address</button>    
                    </Modal>
                }  
            </div>
        );
    }
}

export default App;



